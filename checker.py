import socket
from sock import *
import time
import random
import string
import femida_checker
from femida_checker import Result



class SampleChecker(femida_checker.Checker):

    port = 7777

    def _push(self, team_ip, flag_id, flag):
        # should be a random string with 8-20 length
        login = self.generate_random_string()
        password = self.generate_random_string()
        fileName = self.generate_random_string()
        flag_id = ' '.join([login, password, fileName])

        try:
            sock = Sock("%s:%d" % (team_ip, SampleChecker.port))
        except Exception as ex:
            self.logger.error('Failed o connect to the service')
            self.logger.debug(str(ex), exc_info=True)
            return (Result.DOWN, flag_id)

        try:
            sock.send("0:%s:%s\n" % (login, password))   # create a new user
            sock.send("/:%s:%s\n" % (login, password))   # login
            sock.send("3:%s\n" % (fileName))             # create a new file for flag
            sock.send("7:%s\n" % (fileName))             # choose the file
            sock.send("4:%d|1|%s\n" % (len(flag), flag)) # save flag
        except Exception as ex:
            self.logger.error('Failed to commit the protocol (though the service is responding)')
            self.logger.debug(str(ex), exc_info=True)
            return (Result.MUMBLE, flag_id)

        return (Result.OK, flag_id)


    def generate_random_string(self):
        return ''.join([string.ascii_letters[random.randint(0, len(string.ascii_letters)-1)]  for i in xrange(16)])



    def _pull(self, team_ip, flag_id, flag):
        flag = flag.decode('utf-8').encode('ascii')
        flag_id = flag_id.decode('utf-8').encode('ascii')
        s = flag_id.split()
        (login, password, fileName) = s[0:3]

        try:
            sock = Sock("%s:%d" % (team_ip, SampleChecker.port))
        except Exception as ex:
            self.logger.error('Failed o connect to the service')
            self.logger.debug(str(ex), exc_info=True)
            return Result.DOWN

        try:
            sock.send("/:%s:%s\n" % (login, password))  # login
            sock.send("7:%s\n" % (fileName))            # choose the file
            sock.send("5:\n")                           # get the flag

            sock.read_until("-", timeout=10)
            retrieved_flag = sock.read_until("*", timeout=10)[:-1]
            if flag != retrieved_flag:
                return Result.CORRUPT
        except:
            try:
                sock.send("/:%s:%s\n" % (login, password))  # login
                sock.send("7:%s\n" % (fileName))            # choose the file
                sock.send("5:\n")                           # get the flag

                sock.read_until("-", timeout=10)
                retrieved_flag = sock.read_until("*", timeout=10)[:-1]
                if flag != retrieved_flag:
                    return Result.CORRUPT
            except Exception as ex:
                self.logger.error('Failed to commit the protocol (though the service is responding)')
                self.logger.debug(str(ex), exc_info=True)
                return Result.MUMBLE

        return Result.OK





my_checker = SampleChecker()
my_checker.run()
